package ru.leonova.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.leonova.tm.api.ServiceLocator;
import ru.leonova.tm.api.endpoint.*;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.endpoint.*;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.lang.Exception;
import java.util.*;

public final class Bootstrap implements ServiceLocator {

    private final Scanner scanner = new Scanner(System.in);
    private Session session = null;
    private final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
    private final TaskEndpointService taskEndpointService = new TaskEndpointService();
    private final UserEndpointService userEndpointService = new UserEndpointService();
    private final SessionEndpointService sessionEndpointService = new SessionEndpointService();
    private final DomainEndpointService domainEndpointService = new DomainEndpointService();

    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.leonova.tm").getSubTypesOf(AbstractCommand.class);
    public Bootstrap() {}


    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();


    @Override
    public Session getCurrentSession() {
        return session;
    }

    @Override
    public void setCurrentSession(final Session session) {
        this.session = session;
    }

    @NotNull
    @Override
    public final List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }


    @Override
    public @NotNull IProjectEndpoint getProjectEndpoint() {
        return projectEndpointService.getProjectEndpointPort();
    }

    @Override
    public @NotNull IUserEndpoint getUserEndpoint() {
        return userEndpointService.getUserEndpointPort();
    }

    @Override
    public @NotNull ITaskEndpoint getTaskEndpoint() {
        return taskEndpointService.getTaskEndpointPort();
    }

    @Override
    public @NotNull ISessionEndpoint getSessionEndpoint() {
        return sessionEndpointService.getSessionEndpointPort();
    }

    @Override
    public @NotNull IDomainEndpoint getDomainEndpoint() {
        return domainEndpointService.getDomainEndpointPort();
    }

    private void registry(@NotNull final AbstractCommand command) throws Exception {
        @NotNull final String cliCommand = command.getName();
        @NotNull final String cliDescription = command.getDescription();
        if (cliCommand.isEmpty()) throw new Exception("It is not enumerated");
        if (cliDescription.isEmpty()) throw new Exception("Not description");
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }

    private boolean isCorrectCommand(@NotNull String command) {
        for (@NotNull final AbstractCommand c : commands.values()) {
            if (c.getName().equals(command)) {
                return true;
            }
        }
        return false;
    }

    private void registry(@NotNull final Set<Class<? extends AbstractCommand>> classes) throws Exception {
        for (Class clazz : classes) {
            registry(clazz);
        }
    }

    private void registry(@NotNull final Class... classes) throws Exception {
        for (@NotNull final Class clazz : classes) {
            if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
            @NotNull final Object command = clazz.newInstance();
            @NotNull final AbstractCommand abstractCommand = (AbstractCommand) command;
            registry(abstractCommand);
        }
    }

    public void init() throws Exception {
        registry(classes);
//        initUsers();
        start();
    }

    private void initUsers() throws Exception_Exception {
        getUserEndpoint().addUser("l", "p");
    }

    private void start() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        try {

            @NotNull String command;
            do {
                command = scanner.nextLine();
                execute(command);
            } while (!command.equals("exit"));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    private void execute(@NotNull final String command) throws Exception, EmptyArgumentException {
        if (command.isEmpty() || !isCorrectCommand(command)) return;
        @NotNull final AbstractCommand abstractCommand = commands.get(command);
        @NotNull final boolean secureCheck = !abstractCommand.secure() || (abstractCommand.secure()
//                && getUserEndpoint().isAuth()
        );
        if (secureCheck
//                || getUserEndpoint().getCurrentUser() != null
        ) {
            abstractCommand.execute();
        } else {
            System.out.println("Log in for this command");
        }
    }
}

