package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.User;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.utils.PasswordHashUtil;
import ru.leonova.tm.command.AbstractCommand;

import java.util.Objects;

public final class UserUpdatePasswordCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "up-pass";
    }

    @Override
    public String getDescription() {
        return "Update user password";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("["+getDescription().toUpperCase()+"]");
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final User user = serviceLocator.getUserEndpoint().getUserById(session);
        System.out.println("Enter new password");
        String password = getScanner().nextLine();
        if (password.isEmpty()) throw new Exception("Password is empty");
        password = PasswordHashUtil.md5(password);
        serviceLocator.getUserEndpoint().updatePasswordUser(session, password);
        System.out.println("Password is updated");
    }
}
