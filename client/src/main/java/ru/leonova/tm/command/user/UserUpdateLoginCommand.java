package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.User;
import ru.leonova.tm.command.AbstractCommand;

public final class UserUpdateLoginCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "up-l";
    }

    @Override
    public String getDescription() {
        return "Update login";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("["+getDescription().toUpperCase()+"]");
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final User currentUser = serviceLocator.getUserEndpoint().getUserById(session);
        if(currentUser.getLogin().isEmpty()) return;
        System.out.println("Enter new login:");
        serviceLocator.getUserEndpoint().updateLoginUser(session, getScanner().nextLine());
        System.out.println("Login is updated");
    }
}
