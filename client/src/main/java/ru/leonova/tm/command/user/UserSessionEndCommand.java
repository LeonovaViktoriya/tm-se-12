package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Exception_Exception;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

public final class UserSessionEndCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "session-end";
    }

    @Override
    public String getDescription() {
        return "Завершение сеанса пользователя";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        serviceLocator.getUserEndpoint().logOut(session);
        serviceLocator.setCurrentSession(null);
        System.out.println("Session deleted");
    }
}
