package ru.leonova.tm.command.data.json;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Domain;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

import java.io.File;

public class DomainJaksonSaveToJsonCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jackson-save-json";
    }

    @Override
    public String getDescription() {
        return "Save data to json file via jackson";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainEndpoint().save(session, domain);
        @NotNull final File file = new File("./data/jackson.json");
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();
        objectWriter.writeValue(file, domain);
        System.out.println("Data saved to " + file.getPath());
    }
}
