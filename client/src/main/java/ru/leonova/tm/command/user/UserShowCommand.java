package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.User;
import ru.leonova.tm.command.AbstractCommand;

public final class UserShowCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "show-u";
    }

    @Override
    public String getDescription() {
        return "Viewing the current user profile";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("["+getDescription().toUpperCase()+"]");
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("Session is null");
        @NotNull final User user = serviceLocator.getUserEndpoint().getUserById(session);
        if(user != null) {
            System.out.println("User profile:\nLogin: " + user.getLogin() + ", Password: " + user.getPassword() + ", Id: " + user.getUserId() + ", Role type: " + user.getRoleType());
        }
    }
}
