package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Project;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

import java.util.Collection;

public class ProjectUpdateStatusCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "up-p-s";
    }

    @Override
    public String getDescription() {
        return "Project Update Status";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE STATUS PROJECT]\nList projects");
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final Collection<Project> projectCollection = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session);
        int i = 0;
        for (Project project : projectCollection) {
            i++;
            System.out.println(i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName());
        }
        System.out.println("Enter id project:");
        @NotNull final String projectId = getScanner().nextLine();
        System.out.println("Enter new name for this project:");
        @NotNull final String name = getScanner().nextLine();
        serviceLocator.getProjectEndpoint().updateStatusProject(session, projectId, name);
    }
}
