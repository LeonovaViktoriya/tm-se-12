package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.api.endpoint.User;
import ru.leonova.tm.command.AbstractCommand;

import java.util.List;

public final class UserShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-u";
    }

    @Override
    public String getDescription() {
        return "SHOW LIST USERS";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("["+getDescription()+"]");
        final Session session = serviceLocator.getCurrentSession();
        if(session != null && session.getRoleType().value().equals("admin")){
            @NotNull final List<User> userCollection = serviceLocator.getUserEndpoint().getCollectionUsers(session);
            int i = 0;
            for (@NotNull final User user : userCollection) {
                i++;
                System.out.println(i+". Login: " + user.getLogin() + ", Password: " + user.getPassword() + ", Id: " + user.getUserId() + ", Role type: " + user.getRoleType());
            }
        }else {
            System.out.println("You are not admin");
        }
    }
}
