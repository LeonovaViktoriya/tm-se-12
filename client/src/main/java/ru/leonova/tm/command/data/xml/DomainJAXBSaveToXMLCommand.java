package ru.leonova.tm.command.data.xml;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Domain;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;

public class DomainJAXBSaveToXMLCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "jaxb-save-xml";
    }

    @Override
    public String getDescription() {
        return "Save data to xml with jaxb";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        final Session session = serviceLocator.getCurrentSession();
        @NotNull final File file = new File("./data/domain.xml");
        @NotNull final Domain domain = new Domain();
        serviceLocator.getDomainEndpoint().save(session, domain);
        @NotNull final JAXBContext context = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.marshal(domain, file);
        System.out.println("Saved to "+file.getName());
    }
}
