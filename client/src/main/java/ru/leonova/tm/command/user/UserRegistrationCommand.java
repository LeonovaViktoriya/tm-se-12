package ru.leonova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.User;
import ru.leonova.tm.command.AbstractCommand;
import ru.leonova.tm.utils.PasswordHashUtil;

import java.util.Objects;

public final class UserRegistrationCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "reg";
    }

    @Override
    public String getDescription() {
        return "Registration user";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("["+getDescription().toUpperCase()+"]\nEnter login:");
        @NotNull final String login = getScanner().nextLine();
        System.out.println("Enter password:");
        @NotNull String password = getScanner().nextLine();
        password = Objects.requireNonNull(PasswordHashUtil.md5(password));
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        serviceLocator.getUserEndpoint().addUser(login, password);
        System.out.println("User is registered");
    }
}
