package ru.leonova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.Project;
import ru.leonova.tm.api.endpoint.Session;
import ru.leonova.tm.command.AbstractCommand;

import java.text.SimpleDateFormat;
import java.util.Collection;


public final class ProjectShowListCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "list-p";
    }

    @Override
    public String getDescription() {
        return "Show project list";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws  Exception{
        System.out.println("[PROJECT LIST]");
        final Session session = serviceLocator.getCurrentSession();
        if(session==null) throw new Exception("You are have not session");
        @NotNull final Collection<Project> projectCollection;
        if(session.getRoleType().value().equals("admin")) projectCollection = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session);
        else projectCollection = serviceLocator.getProjectEndpoint().findAllProjectsByUserId(session);
        int i=0;
        for (@NotNull final Project project : projectCollection) {
            if(session.getUserId().equals(project.getUserId())) {
                System.out.println(++i + ". ID PROJECT: " + project.getProjectId() + ", NAME: " + project.getName() + ", ID USER: " + project.getUserId() + ", Date start: " + project.getDateStart() + ", Date end: " +  project.getDateEnd() + ", Date system: " +  project.getDateSystem());
            }
        }
    }
}
