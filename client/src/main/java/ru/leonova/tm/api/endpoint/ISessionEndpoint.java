package ru.leonova.tm.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2020-02-06T10:30:57.937+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.api.tm.leonova.ru/", name = "ISessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ISessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.tm.leonova.ru/ISessionEndpoint/closeSessionRequest", output = "http://endpoint.api.tm.leonova.ru/ISessionEndpoint/closeSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.leonova.ru/ISessionEndpoint/closeSession/Fault/Exception")})
    @RequestWrapper(localName = "closeSession", targetNamespace = "http://endpoint.api.tm.leonova.ru/", className = "ru.leonova.tm.api.endpoint.CloseSession")
    @ResponseWrapper(localName = "closeSessionResponse", targetNamespace = "http://endpoint.api.tm.leonova.ru/", className = "ru.leonova.tm.api.endpoint.CloseSessionResponse")
    public void closeSession(
        @WebParam(name = "session", targetNamespace = "")
        ru.leonova.tm.api.endpoint.Session session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.leonova.ru/ISessionEndpoint/openSessionRequest", output = "http://endpoint.api.tm.leonova.ru/ISessionEndpoint/openSessionResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.leonova.ru/ISessionEndpoint/openSession/Fault/Exception")})
    @RequestWrapper(localName = "openSession", targetNamespace = "http://endpoint.api.tm.leonova.ru/", className = "ru.leonova.tm.api.endpoint.OpenSession")
    @ResponseWrapper(localName = "openSessionResponse", targetNamespace = "http://endpoint.api.tm.leonova.ru/", className = "ru.leonova.tm.api.endpoint.OpenSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.leonova.tm.api.endpoint.Session openSession(
        @WebParam(name = "login", targetNamespace = "")
        java.lang.String login,
        @WebParam(name = "password", targetNamespace = "")
        java.lang.String password
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.tm.leonova.ru/ISessionEndpoint/validRequest", output = "http://endpoint.api.tm.leonova.ru/ISessionEndpoint/validResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.tm.leonova.ru/ISessionEndpoint/valid/Fault/Exception")})
    @RequestWrapper(localName = "valid", targetNamespace = "http://endpoint.api.tm.leonova.ru/", className = "ru.leonova.tm.api.endpoint.Valid")
    @ResponseWrapper(localName = "validResponse", targetNamespace = "http://endpoint.api.tm.leonova.ru/", className = "ru.leonova.tm.api.endpoint.ValidResponse")
    @WebResult(name = "return", targetNamespace = "")
    public boolean valid(
        @WebParam(name = "session", targetNamespace = "")
        ru.leonova.tm.api.endpoint.Session session
    ) throws Exception_Exception;
}
