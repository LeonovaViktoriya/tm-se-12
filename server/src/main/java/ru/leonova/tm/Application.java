package ru.leonova.tm;

import ru.leonova.tm.bootstrap.Bootstrap;
import ru.leonova.tm.exeption.EmptyArgumentException;

import javax.jws.WebService;

@WebService
public class Application {

    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
