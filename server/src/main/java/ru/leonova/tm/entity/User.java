package ru.leonova.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collection;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class User implements Serializable {

    private String login;
    private String password;
    private String userId;
    private String roleType;

    public User(String login, String password){
        this.login = login;
        this.password = password;
        this.userId = UUID.randomUUID().toString();
    }
}
