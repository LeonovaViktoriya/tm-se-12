package ru.leonova.tm.utils;

import com.mysql.jdbc.Connection;
import org.jetbrains.annotations.Nullable;

import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnectionUtil {

    public DbConnectionUtil() {
    }

    @Nullable
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        System.out.println("Драйвер подключен");
        //Создаём соединение
        Connection connection = (Connection)
                DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/task-manager", "root", "root");
        System.out.println("Соединение установлено");
        return connection;
    }

}
