package ru.leonova.tm.service;

import com.mysql.jdbc.Connection;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.api.service.IProjectService;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.repository.ProjectRepository;
import ru.leonova.tm.repository.UserRepository;
import ru.leonova.tm.utils.DbConnectionUtil;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @Override
    public void load(@NotNull final List<Project> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        projectRepository.load(list);
    }

    @Override
    public void create(@NotNull final String userId, @NotNull final Project project) throws Exception {
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        if (connection == null) throw new SQLException("Connection is null");
        @NotNull final ProjectRepository projectRepository;
        projectRepository = new ProjectRepository(connection);
        @NotNull final String projectId = project.getProjectId();
        if (userId.isEmpty() || projectId.isEmpty()) throw new Exception("Id user or Id project is empty!");
        if (!project.getUserId().equals(userId)) throw new Exception("Access denied!");
        if (isExist(projectId)) projectRepository.merge(project);
        else projectRepository.persist(project);
    }

    private boolean isExist(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        return projectRepository.isExist(projectId);
    }

    @Override
    public void updateNameProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String name) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        @NotNull final Project project = projectRepository.findOneById(userId,projectId);
        if(project==null || !project.getUserId().equals(userId)) throw new Exception("Access denied!");
        projectRepository.updateProjectName(projectId, name);
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        return projectRepository.findAllByUserId(userId);
    }

    @Override
    public void deleteProject(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (projectId.isEmpty() || userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        if (project != null && project.getUserId().equals(userId)) projectRepository.remove(userId, project.getProjectId());
    }

    @Override
    public void deleteAllProject(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        projectRepository.removeAllProjectsByUserId(userId);
    }

    @Override
    public List<Project> sortProjectsBySystemDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        return projectRepository.sortedProjectsBySystemDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStartDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        return projectRepository.sortedProjectsByStartDate(userId);
    }

    @Override
    public List<Project> sortProjectsByEndDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        return projectRepository.sortedProjectsByEndDate(userId);
    }

    @Override
    public List<Project> sortProjectsByStatus(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        return projectRepository.sortedProjectsByStatus(userId);
    }

    @Override
    public Project searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws Exception {
        if (userId.isEmpty() || projectName.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        return projectRepository.searchProjectByName(userId, projectName);
    }

    @Override
    public Project searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws Exception {
        if (userId.isEmpty() || description.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        return projectRepository.searchProjectByDescription(description, userId);
    }

    @Override
    public void updateStatusProject(@NotNull String userId, @NotNull String projectId, @NotNull String name) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty() || name.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final ProjectRepository projectRepository;
        if (connection == null) throw new SQLException("Connection is null");
        projectRepository = new ProjectRepository(connection);
        projectRepository.updateStatusName(projectId, name);
    }
}
