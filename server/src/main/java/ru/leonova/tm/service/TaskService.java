package ru.leonova.tm.service;

import com.mysql.jdbc.Connection;
import lombok.Cleanup;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.repository.TaskRepository;
import ru.leonova.tm.utils.DbConnectionUtil;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    @Override
    public void create(@NotNull final String userId, @NotNull final Task task) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        if (task.getUserId().equals(userId)) taskRepository.merge(task);
    }

    @Override
    public void load(@NotNull List<Task> list) throws Exception {
        if (list.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        taskRepository.load(list);
    }

    @Override
    public void updateTaskName(@NotNull String userId, @NotNull final String taskId, @NotNull final String taskName) throws Exception {
        if (taskName.isEmpty() || taskId.isEmpty() || taskId.equals(userId)) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        taskRepository.updateTaskName(taskId, taskName);
    }

    @Override
    public void deleteTaskByIdProject(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        if (projectId.isEmpty() || userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        taskRepository.removeByIdProject(userId, projectId);
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        return taskRepository.findAllByUserId(userId);
    }

    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        return taskRepository.findAllByProjectId(projectId);
    }


    @Override
    public void deleteTaskByIdUser(@NotNull final String userId, @NotNull final String taskId) throws Exception {
        if (taskId.isEmpty() || userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        taskRepository.removeByIdUser(userId);
    }


    @Override
    public void deleteAllTaskByUserId(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        taskRepository.removeAllTasksCollectionByUserId(userId);
    }

    @Override
    public void deleteAllTaskByProjectId(@NotNull String userId, @NotNull final String projectId) throws Exception {
        if (userId.isEmpty() || projectId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        taskRepository.removeAllTasksCollectionByProjectId(userId, projectId);
    }

    @Override
    public Task findTaskByName(@NotNull final String userId, @NotNull final String taskName) throws Exception {
        if (userId.isEmpty() || taskName.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        return taskRepository.findOneByName(userId, taskName);
    }

    @Override
    public List<Task> sortTasksByEndDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        return taskRepository.sortByEndDate(userId);
    }

    @Override
    public List<Task> sortTasksByStartDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        return taskRepository.sortByStartDate(userId);
    }

    @Override
    public List<Task> sortTasksBySystemDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        return taskRepository.sortBySystemDate(userId);
    }

    @Override
    public List<Task> sortTasksByStatus(String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final TaskRepository taskRepository;
        if (connection == null) throw new SQLException("Connection is null");
        taskRepository = new TaskRepository(connection);
        return taskRepository.sortByStatus(userId);
    }

}
