package ru.leonova.tm.service;

import com.mysql.jdbc.Connection;
import lombok.Cleanup;
import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.repository.UserRepository;
import ru.leonova.tm.utils.DbConnectionUtil;
import ru.leonova.tm.utils.PasswordHashUtil;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    private User currentUser;

    @Override
    public void create(@NotNull final User user) throws SQLException, ClassNotFoundException {
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final UserRepository userRepository;
        if (connection == null) throw new SQLException("Connection is null");
        userRepository = new UserRepository(connection);
        user.setRoleType(RoleType.USER.getRole());
        userRepository.persist(user);
    }

    @Override
    public void load(@NotNull List<User> list) throws Exception {
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final UserRepository userRepository;
        if (connection == null) throw new SQLException("Connection is null");
        userRepository = new UserRepository(connection);
        if (list.isEmpty()) throw new Exception();
        userRepository.load(list);
    }

    @Override
    public void logOut(@NotNull final User user) {
        currentUser = null;
    }

    @Override
    public Collection<User> getCollection() throws Exception {
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final UserRepository userRepository;
        if (connection == null) return null;
        userRepository = new UserRepository(connection);
        return userRepository.findAll();
    }

    @Override
    public void updateLogin(@NotNull final User user, @NotNull final String login) throws Exception {
        if(login.isEmpty())throw new Exception("login is empty!");
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final UserRepository userRepository;
        if (connection == null) return;
        userRepository = new UserRepository(connection);
        userRepository.updateLogin(user, login);
    }

    @Override
    public User authorizationUser(@NotNull final String login, @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception("login or password is empty!");
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final UserRepository userRepository;
        if (connection == null) return null;
        userRepository = new UserRepository(connection);
        @NotNull final User user = userRepository.findByLoginAndPassword(login,password);
        return user;
    }

    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public User getById(@NotNull final String userId) throws Exception {
        if(userId.isEmpty())throw new Exception("Id user is empty!");
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final UserRepository userRepository;
        if (connection == null) return null;
        userRepository = new UserRepository(connection);
        if (userId.isEmpty()) throw new Exception();
        return userRepository.findOne(userId);
    }

    @Override
    public String md5Apache(@NotNull final String password) throws Exception {
        if (password.isEmpty()) throw new Exception("password is empty!");
        return DigestUtils.md5Hex(password);
    }

    @Override
    public void addAll(@NotNull List<User> users) throws Exception {
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final UserRepository userRepository;
        if (connection == null) return;
        userRepository = new UserRepository(connection);
        users.addAll(userRepository.findAll());
    }

    @Override
    public void updatePassword(@NotNull User user, @NotNull String password) throws Exception {
        if (password.isEmpty()) throw new Exception("password is empty!");
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final UserRepository userRepository;
        if (connection == null) return;
        userRepository = new UserRepository(connection);
        userRepository.updatePassword(user, password);
    }

    @Override
    public void adminRegistration(@NotNull final String login, @NotNull String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception("login or password is empty!");
        @Cleanup Connection connection = DbConnectionUtil.getConnection();
        @NotNull final UserRepository userRepository;
        if (connection == null) return;
        userRepository = new UserRepository(connection);
        @NotNull final User admin = new User("admin", md5Apache("admin"));
        admin.setRoleType(RoleType.ADMIN.getRole());
        userRepository.persist(admin);
    }

}
