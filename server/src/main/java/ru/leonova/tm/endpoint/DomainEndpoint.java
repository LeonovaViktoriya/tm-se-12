package ru.leonova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.IDomainEndpoint;
import ru.leonova.tm.api.service.IDomainService;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.entity.Domain;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.leonova.tm.api.endpoint.IDomainEndpoint")
public class DomainEndpoint implements IDomainEndpoint {
    private IDomainService domainService;
    private ISessionService sessionService;

    public static final String DOMAIN_URL = "http://localhost:8080/DomainEndpoint?wsdl";

    public DomainEndpoint(IDomainService domainService, ISessionService sessionService) {
        this.domainService = domainService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void save(@WebParam(name = "session") @NotNull Session session, @NotNull Domain domain) throws Exception {
        sessionService.valid(session);
//        domainService.save(domain);
    }

    @Override
    @WebMethod
    public void load(@WebParam(name = "session") @NotNull Session session, @NotNull Domain domain) throws Exception {
        sessionService.valid(session);
//        domainService.load(domain);
    }
}
