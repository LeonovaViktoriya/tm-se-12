package ru.leonova.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.IUserEndpoint;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.api.service.IUserService;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService(endpointInterface = "ru.leonova.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    private IUserService userService;
    private ISessionService sessionService;

    public UserEndpoint(IUserService userService, ISessionService sessionService) {
        this.userService = userService;
        this.sessionService = sessionService;
    }

    public static final String URL_USER = "http://localhost:8080/UserEndpoint?wsdl";

    @WebMethod
    @Override
    public boolean isAuth(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.valid(session);
        return userService.isAuth();
    }

    @WebMethod
    @Override
    public void logOut(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.valid(session);
        userService.logOut(userService.getById(session.getUserId()));
        sessionService.closeSession(session);
    }

    @WebMethod
    @Override
    public void addUser(@NotNull @WebParam(name = "login", partName = "login") final String login, @NotNull @WebParam(name = "password", partName = "password") final String password) throws Exception {
        User user = new User(login, password);
        userService.create(user);
    }

    @Override
    public void loadUserList(@WebParam(name = "session") @NotNull Session session, @NotNull List<User> list) throws Exception {
        sessionService.valid(session);
        userService.load(list);
    }

    @WebMethod
    @Override
    public Collection<User> getCollectionUsers(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.valid(session);
        return userService.getCollection();
    }

    @WebMethod
    @Override
    @SneakyThrows
    public Session authorizationUserAndOpenSession(@WebParam(name = "login") @NotNull final String login, @WebParam(name = "password") @NotNull final String password) throws Exception {
        if (login.isEmpty() || password.isEmpty()) throw new Exception();
        User user = userService.authorizationUser(login, password);
        if (user == null) throw new Exception("You are not authorized");
        return sessionService.openSession(login, password);
    }

    @WebMethod
    @Override
    public User getUserById(@WebParam(name = "session") @NotNull Session session) throws Exception{
        sessionService.valid(session);
        if (session.getUserId().isEmpty()) throw new Exception();
        return userService.getById(session.getUserId());
    }

    @WebMethod
    @Override
    public void addAllUsers(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "userList") @NotNull final List<User> users) throws Exception {
        sessionService.valid(session);
        if (users.isEmpty()) throw new EmptyCollectionException();
        userService.addAll(users);
    }

    @WebMethod
    @Override
    public void updateLoginUser(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "login") @NotNull String login) throws Exception {
        sessionService.valid(session);
        @NotNull final User user = userService.getById(session.getUserId());
        if (user==null) throw new Exception();
        userService.updateLogin(user,login);
    }

    @WebMethod
    @Override
    public void updatePasswordUser(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "password") @NotNull String password) throws Exception {
        sessionService.valid(session);
        @NotNull final User user = userService.getById(session.getUserId());
        if (user==null) throw new Exception();
        userService.updatePassword(user,password);
    }
}
