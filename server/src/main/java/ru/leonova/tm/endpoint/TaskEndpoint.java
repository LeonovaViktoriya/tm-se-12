package ru.leonova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.ITaskEndpoint;
import ru.leonova.tm.api.service.ISessionService;
import ru.leonova.tm.api.service.ITaskService;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyCollectionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@WebService(endpointInterface = "ru.leonova.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    private ITaskService taskService;
    private ISessionService sessionService;

    public TaskEndpoint(ITaskService taskService, ISessionService sessionService) {
        this.taskService = taskService;
        this.sessionService = sessionService;
    }

    public TaskEndpoint() {
    }

    public static final String URL_TASK = "http://localhost:8080/TaskEndpoint?wsdl";

    @WebMethod
    @Override
    public void createTask(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "task") @NotNull Task task,
                           @WebParam(name = "dateStart") XMLGregorianCalendar dateStart, @WebParam(name = "dateEnd") XMLGregorianCalendar dateEnd,
                           @WebParam(name = "dateSystem") @NotNull XMLGregorianCalendar dateSystem) throws Exception {
        Date dateStartDate = dateStart.toGregorianCalendar().getTime();
        Date dateEndDate = dateEnd.toGregorianCalendar().getTime();
        Date dateSystemDate = dateSystem.toGregorianCalendar().getTime();
        task.setDateStart(dateStartDate);
        task.setDateEnd(dateEndDate);
        task.setTaskId(UUID.randomUUID().toString());
        task.setDateSystem(dateSystemDate);
        sessionService.valid(session);
        if (session.getUserId().isEmpty()) throw new Exception();
        taskService.create(session.getUserId(), task);
    }

    @Override
    @WebMethod
    public void loadListTask(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "list") @NotNull List<Task> list) throws Exception {
        sessionService.valid(session);
        if (list.isEmpty()) throw new EmptyCollectionException();
        taskService.load(list);
    }

    @Override
    @WebMethod
    public void updateTaskName(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "taskId") @NotNull String taskId, @WebParam(name = "taskName") @NotNull String taskName) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty() || taskId.isEmpty() || taskName.isEmpty()) throw new Exception();
        taskService.updateTaskName(session.getUserId(), taskId, taskName);
    }

    @Override
    @WebMethod
    public void deleteTasksByIdProject(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "projectId") @NotNull String projectId) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty() || projectId.isEmpty()) throw new Exception();
        taskService.deleteTaskByIdProject(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public Collection<Task> findAllTasksByUserId(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty()) throw new Exception();
        return taskService.findAllByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public Collection<Task> findAllTasksByProjectId(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "projectId") @NotNull String projectId) throws Exception {
        sessionService.valid(session);
        if (projectId.isEmpty()) throw new Exception();
        return taskService.findAllByProjectId(projectId);
    }


    @Override
    @WebMethod
    public void deleteTask(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "taskId") @NotNull String taskId) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty() || taskId.isEmpty()) throw new Exception();
        taskService.deleteTaskByIdUser(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    public void deleteAllTaskByUserId(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty()) throw new Exception();
        taskService.deleteAllTaskByUserId(session.getUserId());
    }

    @Override
    @WebMethod
    public void deleteAllTaskByProjectId(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "projectId") @NotNull String projectId) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty() || projectId.isEmpty()) throw new Exception();
        taskService.deleteAllTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    public Task findTaskByName(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "taskName") @NotNull String taskName) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty() || taskName.isEmpty()) throw new Exception();
        return taskService.findTaskByName(session.getUserId(), taskName);
    }

    @Override
    @WebMethod
    public List<Task> sortTasksByEndDate(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty()) throw new Exception();
        return taskService.sortTasksByEndDate(session.getUserId());
    }

    @Override
    @WebMethod
    public List<Task> sortTasksByStartDate(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty()) throw new Exception();
        return taskService.sortTasksByStartDate(session.getUserId());
    }

    @Override
    @WebMethod
    public List<Task> sortTasksBySystemDate(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty()) throw new Exception();
        return taskService.sortTasksBySystemDate(session.getUserId());
    }

    @Override
    @WebMethod
    public List<Task> sortTasksByStatus(@WebParam(name = "session") @NotNull Session session) throws Exception {
        sessionService.valid(session);
        if (session.getUserId().isEmpty()) throw new Exception();
        return taskService.sortTasksByStatus(session.getUserId());
    }

//    @Override
//    @WebMethod
//    public void addAllTasks(@WebParam(name = "tasks") @NotNull final List<Task> tasks) throws Exception {
//        if (tasks.isEmpty()) throw new EmptyCollectionException();
//        taskService.addAll(tasks);
//    }

}
