package ru.leonova.tm.repository;

import com.mysql.jdbc.Connection;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.IProjectRepository;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.utils.DbConnectionUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private final Connection connection;

    public ProjectRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    @SneakyThrows
    private Project fetch(@NotNull final ResultSet row) {
        @NotNull final Project project = new Project();
        project.setProjectId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setUserId(row.getString("user_id"));
        project.setStatus(row.getString("statusType"));
        project.setDateStart(row.getTimestamp("beginDate"));
        project.setDateEnd(row.getTimestamp("endDate"));
        project.setDateSystem(row.getTimestamp("createDate"));
        return project;
    }

    @Override
    public void persist(@NotNull final Project project) throws SQLException {
        @NotNull final String query = "INSERT INTO app_project (id, name, description, statusType, beginDate, endDate, createDate, user_id) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        java.sql.Timestamp sqlDateStart = new java.sql.Timestamp(project.getDateStart().getTime());
        java.sql.Timestamp sqlDateEnd = new java.sql.Timestamp(project.getDateEnd().getTime());
        java.sql.Timestamp sqlDateSystem = new java.sql.Timestamp(project.getDateSystem().getTime());
        preparedStatement.setString(1, project.getProjectId());
        preparedStatement.setString(2, project.getName());
        preparedStatement.setString(3, project.getDescription());
        preparedStatement.setString(4, project.getStatus());
        preparedStatement.setTimestamp(5, sqlDateStart);
        preparedStatement.setTimestamp(6, sqlDateEnd);
        preparedStatement.setTimestamp(7, sqlDateSystem);
        preparedStatement.setString(8, project.getUserId());
        preparedStatement.execute();
    }

    @Override
    public Project findOneById(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id=? AND id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This user does not at database");
        Project project = null;
        while (resultSet.next()) project = fetch(resultSet);
        return project;
    }

    @Override
    public Collection<Project> findAllByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This user does not at database");
        @NotNull final Collection<Project> projectCollection = new ArrayList<>();
        while (resultSet.next()) (projectCollection).add(fetch(resultSet));
        return projectCollection;
    }

    @Override
    public void updateProjectName(@NotNull final String projectId, @NotNull final String name) throws Exception {
        @NotNull final String query = "UPDATE app_project SET name = ? WHERE id = ?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, projectId);
        preparedStatement.executeUpdate();
    }

    public boolean isExist(@NotNull final String projectId) throws Exception {
        if (projectId.isEmpty()) throw new Exception();
        return storage.containsKey(projectId);
    }

    @Override
    public void merge(@NotNull final Project project) throws Exception {
        if (project.getProjectId() == null || project.getProjectId().isEmpty()) throw new Exception();
        for (@NotNull final Project project1 : storage.values()) {
            if (project1.getProjectId().equals(project.getProjectId())) {
                updateProjectName(project1.getProjectId(), project.getName());
            } else {
                persist(project);
            }
        }
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String query = "DELETE FROM app_project WHERE user_id=? AND id = ?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectId);
        preparedStatement.execute();
    }

    @Override
    public void removeAllProjectsByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String query = "DELETE FROM app_project WHERE user_id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.execute();
    }

    @Override
    public List<Project> sortedProjectsBySystemDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projectCollection = findAllByUserId(userId);
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateSystem);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByStartDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projectCollection = findAllByUserId(userId);
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projectCollection.isEmpty() || projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateStart);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByEndDate(@NotNull final String userId) throws Exception, EmptyCollectionException {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projectCollection = Objects.requireNonNull(findAllByUserId(userId));
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getDateEnd);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public List<Project> sortedProjectsByStatus(@NotNull final String userId) throws EmptyCollectionException, Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Project> projectCollection = Objects.requireNonNull(findAllByUserId(userId));
        @NotNull final List<Project> projects = new ArrayList<>(projectCollection);
        if (projectCollection.isEmpty() || projects.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Project> comparator = Comparator.comparing(Project::getStatus);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project searchProjectByName(@NotNull final String userId, @NotNull final String projectName) throws Exception {
        @NotNull final String query = "SELECT * FROM app_project WHERE user_id=? AND name=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, userId);
        preparedStatement.setString(2, projectName);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This user does not at database");
        Project project = null;
        while (resultSet.next()) project = fetch(resultSet);
        return project;
    }

    @Override
    public Project searchProjectByDescription(@NotNull final String userId, @NotNull final String description) throws Exception {
        @NotNull final String query = "SELECT * app_project WHERE user_id = ? AND description = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, description);
        preparedStatement.setString(2, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This user does not at database");
        Project project = null;
        while (resultSet.next()) project = fetch(resultSet);
        return project;
    }

    @Override
    public void load(@NotNull final List<Project> list) {
        list.forEach(project -> storage.put(project.getProjectId(), project));
    }
    @Override
    public void updateStatusName(String projectId, String status) throws SQLException {
        @NotNull final String query = "UPDATE app_project SET statusType = ? WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, status);
        preparedStatement.setString(2, projectId);
        preparedStatement.executeUpdate();
    }
}
