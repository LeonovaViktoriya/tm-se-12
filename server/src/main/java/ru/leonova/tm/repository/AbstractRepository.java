package ru.leonova.tm.repository;

import com.mysql.jdbc.Connection;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.util.LinkedHashMap;
import java.util.Map;

abstract class AbstractRepository<E> {

    final Map<String, E> storage = new LinkedHashMap<>();


    @Nullable
    Connection connection;

//    public AbstractRepository(Connection connection) {
//        this.connection = connection;
//    }

    abstract void persist(@NotNull E e) throws Exception;

    abstract void merge(@NotNull E e) throws Exception;
}
