package ru.leonova.tm.repository;

import com.mysql.jdbc.Connection;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.endpoint.RoleType;
import ru.leonova.tm.api.repository.IUserRepository;
import ru.leonova.tm.entity.User;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final Connection connection;

    public UserRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    @SneakyThrows
    private User fetch(@NotNull final ResultSet row) throws SQLException {
        @NotNull final User user = new User();
        user.setUserId(row.getString("id"));
        user.setRoleType(row.getString("role"));
        user.setLogin(row.getString("login"));
        user.setPassword(row.getString("password"));
        return user;
    }

    @Override
    public void persist(@NotNull final User user) throws SQLException {
        @NotNull final String query = "INSERT INTO app_user (id, login, password, role) VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, user.getUserId());
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.setString(3, user.getPassword());
        preparedStatement.setString(4, user.getRoleType());
        preparedStatement.execute();
    }

    @Override
    void merge(@NotNull final User user) throws Exception {
        @NotNull final String userId = user.getUserId();
        if (!isExist(userId)) persist(user);
        else {
            @NotNull final String query = "UPDATE app_user SET login=?, password=? WHERE id=?";
            @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(2, user.getUserId());
            preparedStatement.execute();
        }
    }
    @Override
    public void updateLogin(@NotNull final User user, @NotNull final String login) throws SQLException {
        @NotNull final String query = "UPDATE app_user SET login = ? WHERE login = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, user.getLogin());
        preparedStatement.executeUpdate();
    }

    private boolean isExist(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        return storage.containsKey(userId);
    }

    @Override
    public User findOne(@NotNull final String userId) throws Exception {
        @NotNull User user = new User();
        @NotNull final String query = "SELECT * FROM app_user WHERE id=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This user does not at database");
        while (resultSet.next()) user = fetch(resultSet);
        preparedStatement.close();
        if (!userId.equals(user.getUserId())) throw new Exception("Access denied.");
        return user;
    }

    @Override
    public User findByLoginAndPassword(@NotNull final String login, @NotNull final String password) throws Exception {
        User user = null;
        @NotNull final String query = "SELECT * FROM app_user WHERE login = ? AND password = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, login);
        preparedStatement.setString(2, password);
        @NotNull ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This user does not at database");
        while (resultSet.next()) user = fetch(resultSet);
        return user;
    }

    @Override
    public Collection<User> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM app_user";
        @NotNull final Collection<User> users = new ArrayList<>();
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            @NotNull final User user = fetch(resultSet);
            users.add(user);
        }
        preparedStatement.close();
        return users;
    }

    @Override
    public void remove(@NotNull final User user) throws Exception {
        @NotNull final String query = "DELETE FROM app_user WHERE id=?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, user.getUserId());
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void removeAll() throws SQLException {
        @NotNull final String query = "DELETE FROM app_user";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.execute();
        preparedStatement.close();
    }

    @Override
    public void load(@NotNull final List<User> list) throws Exception {
        for (@NotNull final User user : list) {
            merge(user);
        }
    }

    @Override
    public void updatePassword(@NotNull final User user, @NotNull final String password) throws Exception{
        @NotNull final String query = "UPDATE app_user SET password = ? WHERE password = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, password);
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.executeUpdate();
    }
}
