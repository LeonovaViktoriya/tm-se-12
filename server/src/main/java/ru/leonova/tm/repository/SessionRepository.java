package ru.leonova.tm.repository;

import com.mysql.jdbc.Connection;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.api.repository.ISessionRepository;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final Connection connection;

    public SessionRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }


    @Override
    public void persist(@NotNull final Session session){
        storage.put(session.getSessionId(), session);
    }

    @Override
    public void merge(@NotNull final Session session) throws Exception {
        if (session.getSessionId().isEmpty()) throw new Exception();
        for (@NotNull final Session s : storage.values()) {
            if (s.getSessionId().equals(session.getSessionId())) {
                update(s.getSessionId(), session.getRoleType(), session.getUserId(), session.getSignature(), session.getTimestamp());
            } else {
                persist(session);
            }
        }
    }

    @Override
    public void update(String sessionId, RoleType roleType, String userId, String signature, Long timestamp) throws SQLException {
        @NotNull final String query = "UPDATE app_session SET roleType = ?, user_id=?, signature=?, timestamp=?  WHERE id = ?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, roleType.getRole());
        preparedStatement.setString(2, userId);
        preparedStatement.setString(3, signature);
        preparedStatement.setLong(4, timestamp);
        preparedStatement.setString(5, sessionId);
        preparedStatement.executeUpdate();
    }

    @NotNull
    @SneakyThrows
    private Session fetch(@NotNull final ResultSet row) {
        @NotNull final Session session = new Session();
        session.setSessionId(row.getString("id"));
        session.setTimestamp(row.getLong("timestamp"));
        session.setSignature(row.getString("signature"));
        session.setUserId(row.getString("user_id"));
        return session;
    }

    @Override
    public Session findSessionById(String sessionId) throws Exception {
        @NotNull final String query = "SELECT FROM app_session WHERE id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, sessionId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This user does not at database");
        Session session = null;
        while (resultSet.next()) session = fetch(resultSet);
        return session;
    }


    @Override
    public Session findSessionByUserId(String userId) throws Exception {
        @NotNull final String query = "SELECT FROM app_session WHERE user_id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This user does not at database");
        Session session = null;
        while (resultSet.next()) session = fetch(resultSet);
        return session;
    }


    @Override
    public void remove(@NotNull final String sessionId) throws Exception {
        @NotNull final String query = "DELETE FROM app_session WHERE id = ?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, sessionId);
        preparedStatement.execute();
    }

}
