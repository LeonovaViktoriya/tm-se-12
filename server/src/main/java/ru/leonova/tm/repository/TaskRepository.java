package ru.leonova.tm.repository;

import com.mysql.jdbc.Connection;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyCollectionException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ru.leonova.tm.api.repository.ITaskRepository {

    @NotNull
    private final Connection connection;

    public TaskRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @Override
    public void persist(@NotNull final Task task) throws Exception {
        @NotNull final String query = "INSERT INTO app_task (id, name, description, statusType, beginDate, endDate, createDate, user_id, project_id) VALUES (?, ?, ?, ?)";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        java.sql.Timestamp sqlDateStart = new java.sql.Timestamp(task.getDateStart().getTime());
        java.sql.Timestamp sqlDateEnd = new java.sql.Timestamp(task.getDateEnd().getTime());
        java.sql.Timestamp sqlDateSystem = new java.sql.Timestamp(task.getDateSystem().getTime());
        preparedStatement.setString(1, task.getTaskId());
        preparedStatement.setString(2, task.getName());
        preparedStatement.setString(3, task.getDescription());
        preparedStatement.setString(4, task.getStatus());
        preparedStatement.setTimestamp(5, sqlDateStart);
        preparedStatement.setTimestamp(6, sqlDateEnd);
        preparedStatement.setTimestamp(7, sqlDateSystem);
        preparedStatement.setString(8, task.getUserId());
        preparedStatement.setString(8, task.getProjectId());
        preparedStatement.execute();
    }

    @Override
    public void merge(@NotNull final Task task) throws Exception {
        @NotNull final String taskId = task.getTaskId();
        if(taskId.isEmpty()) throw new Exception();
        if (isExist(taskId)){
            @NotNull final Task taskUpdate = storage.get(taskId);
            taskUpdate.setName(task.getName());
        }
        else persist(task);
    }

    @Override
    public boolean isExist(@NotNull final String taskId) throws Exception {
        if (taskId.isEmpty()) throw new Exception();
        return storage.containsKey(taskId);
    }

    @NotNull
    @SneakyThrows
    private Task fetch(@NotNull final ResultSet row) {
        @NotNull final Task task = new Task();
        task.setTaskId(row.getString("id"));
        task.setProjectId(row.getString("project-id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setUserId(row.getString("user_id"));
        task.setStatus(row.getString("statusType"));
        task.setDateStart(row.getTimestamp("beginDate"));
        task.setDateEnd(row.getTimestamp("endDate"));
        task.setDateSystem(row.getTimestamp("createDate"));
        return task;
    }

    @Override
    public Task findOneById(@NotNull final String taskId) throws Exception {
        @NotNull final String query = "SELECT * FROM app_task WHERE id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, taskId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This task does not at database");
        Task task = null;
        while (resultSet.next()) task = fetch(resultSet);
        return task;
    }

    @Override
    public Collection<Task> findAllByUserId(@NotNull final String userId) throws Exception {
        @NotNull final String query = "SELECT * FROM app_task WHERE user_id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This task does not at database");
        @NotNull final Collection<Task> taskCollection = new ArrayList<>();
        while (resultSet.next()) (taskCollection).add(fetch(resultSet));
        return taskCollection;
    }

    @Override
    public void updateTaskName(@NotNull final String taskId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "UPDATE app_project SET name = ? WHERE id = ?";
        @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, taskId);
        preparedStatement.executeUpdate();
    }

    @Override
    public Collection<Task> findAllByProjectId(@NotNull final String projectId) throws Exception {
        @NotNull final String query = "SELECT * FROM app_task WHERE project_id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, projectId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This task does not at database");
        @NotNull final Collection<Task> taskCollection = new ArrayList<>();
        while (resultSet.next()) (taskCollection).add(fetch(resultSet));
        return taskCollection;
    }

    @Override
    public void removeByIdUser(@NotNull final String userId) throws Exception {
        @NotNull final String query = "DELETE FROM app_task WHERE user_id = ?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, userId);
        preparedStatement.executeQuery();
    }

    @Override
    public void removeByIdProject(@NotNull String userId, @NotNull final String projectId) throws Exception {
        @NotNull final String query = "DELETE FROM app_task WHERE project_id = ? AND user_id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, projectId);
        preparedStatement.setString(2, userId);
        preparedStatement.executeQuery();
    }

    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String taskName) throws Exception {
        @NotNull final String query = "SELECT * FROM app_task WHERE name=? AND user_id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, taskName);
        preparedStatement.setString(2, userId);
        @NotNull final ResultSet resultSet = preparedStatement.executeQuery();
        if(resultSet==null) throw new Exception("This task does not at database");
        Task task = null;
        while (resultSet.next()) task = fetch(resultSet);
        return task;
    }

    @Override
    public List<Task> sortByEndDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getDateEnd);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> sortByStartDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getDateStart);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> sortBySystemDate(@NotNull final String userId) throws Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getDateSystem);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public List<Task> sortByStatus(@NotNull final String userId) throws  Exception {
        if (userId.isEmpty()) throw new Exception();
        @NotNull final Collection<Task> taskCollection = findAllByUserId(userId);
        if (taskCollection.isEmpty()) throw new EmptyCollectionException();
        @NotNull final List<Task> taskList = new ArrayList<>(taskCollection);
        if (taskList.isEmpty()) throw new EmptyCollectionException();
        @NotNull final Comparator<Task> comparator = Comparator.comparing(Task::getStatus);
        taskList.sort(comparator);
        return taskList;
    }

    @Override
    public void load(@NotNull final List<Task> list) {
        list.forEach(task -> storage.put(task.getTaskId(), task));
    }

    @Override
    public void removeAllTasksCollectionByUserId(@NotNull final String userId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE user_id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, userId);
        preparedStatement.executeQuery();
    }

    @Override
    public void removeAllTasksCollectionByProjectId(@NotNull final String userId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = "DELETE FROM app_task WHERE project_id=? AND user_id=?";
        @Cleanup @NotNull final PreparedStatement preparedStatement = connection.prepareStatement(query);
        if(preparedStatement==null) throw new SQLException();
        preparedStatement.setString(1, projectId);
        preparedStatement.setString(2, userId);
        preparedStatement.executeQuery();
    }

  }

