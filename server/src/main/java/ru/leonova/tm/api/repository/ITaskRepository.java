package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface ITaskRepository {
    void persist(@NotNull Task task) throws Exception;

    void merge(@NotNull Task task) throws Exception;

    Task findOneById(@NotNull String taskId) throws Exception;

    Collection<Task> findAllByUserId(@NotNull String userId) throws Exception;

    void updateTaskName(@NotNull String taskId, @NotNull String name) throws SQLException;

    Collection<Task> findAllByProjectId(@NotNull String projectId) throws Exception;

    void removeByIdUser(@NotNull String userId) throws Exception;

    boolean isExist(@NotNull String taskId) throws Exception;

    void removeByIdProject(@NotNull String userId, @NotNull String taskId) throws Exception;

    Task findOneByName(@NotNull String userId, @NotNull String taskName) throws Exception;

    List<Task> sortByEndDate(@NotNull String userId) throws Exception;

    List<Task> sortByStartDate(@NotNull String userId) throws Exception;

    List<Task> sortBySystemDate(@NotNull String userId) throws Exception;

    List<Task> sortByStatus(@NotNull String userId) throws Exception;

    void load(@NotNull List<Task> list);

    void removeAllTasksCollectionByUserId(@NotNull String userId) throws SQLException;

    void removeAllTasksCollectionByProjectId(@NotNull String userId, @NotNull String projectId) throws SQLException;
}
