package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IUserService {

    void create(@NotNull User user) throws SQLException, ClassNotFoundException;

    void load(@NotNull List<User> list) throws Exception;

    void updateLogin(@NotNull User user, @NotNull String login) throws SQLException, ClassNotFoundException, Exception;

    User authorizationUser(@NotNull String login, @NotNull String password) throws Exception;

    boolean isAuth();

    User getById(@NotNull String userId) throws Exception;

    void logOut(@NotNull User user);

    Collection<User> getCollection() throws SQLException, Exception;

    void adminRegistration(@NotNull String admin, @NotNull String admin1) throws Exception;

    String md5Apache(@NotNull String password) throws Exception;

    void addAll(@NotNull List<User> users) throws SQLException, Exception;

    void updatePassword(@NotNull User user, @NotNull String password) throws SQLException, ClassNotFoundException, Exception;
}
