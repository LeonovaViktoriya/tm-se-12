package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IProjectRepository {

    void persist(@NotNull Project project) throws SQLException, ClassNotFoundException;

//    Collection<Project> findAll();

    Project findOneById(@NotNull String userId, @NotNull String projectId) throws Exception;

    void merge(@NotNull Project project) throws Exception;

    void remove(@NotNull String userId, @NotNull String projectId) throws Exception;

    void removeAllProjectsByUserId(@NotNull String userId) throws Exception;

    Collection<Project> findAllByUserId(@NotNull String userId) throws Exception;

    void updateProjectName(@NotNull String projectId, @NotNull String name) throws Exception;

    boolean isExist(@NotNull String projectId) throws Exception;

    List<Project>  sortedProjectsBySystemDate(@NotNull String userId) throws Exception;

    List<Project> sortedProjectsByStartDate(@NotNull String userId) throws Exception;

    List<Project> sortedProjectsByEndDate(@NotNull String userId) throws Exception;

    List<Project> sortedProjectsByStatus(@NotNull String userId) throws Exception;

    Project searchProjectByName(@NotNull String userId, @NotNull String projectName) throws Exception;

    Project searchProjectByDescription(@NotNull String userId, @NotNull String description) throws Exception;

    void load(@NotNull List<Project> list) throws Exception;

    void updateStatusName(String projectId, String name) throws SQLException;
}
