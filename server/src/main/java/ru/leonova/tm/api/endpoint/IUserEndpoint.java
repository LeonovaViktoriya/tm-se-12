package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;
import ru.leonova.tm.exeption.EmptyCollectionException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
public interface IUserEndpoint {

//    @WebMethod
//    User getCurrentUser(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    boolean isAuth(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    void logOut(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    void addUser(@WebParam(name = "l") @NotNull String l, @WebParam(name = "p") @NotNull String p) throws java.lang.Exception;

    @WebMethod
    void loadUserList(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "list") @NotNull List<User> list) throws java.lang.Exception;

    @WebMethod
    Collection<User> getCollectionUsers(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    Session authorizationUserAndOpenSession(@WebParam(name = "login") @NotNull String login, @WebParam(name = "password") @NotNull String password) throws java.lang.Exception;

    @WebMethod
    User getUserById(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    void addAllUsers(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "users") @NotNull List<User> users) throws java.lang.Exception;

    @WebMethod
    void updateLoginUser(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "login") @NotNull String login) throws java.lang.Exception;

    @WebMethod
    void updatePasswordUser(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "password") @NotNull String password) throws java.lang.Exception;
}
