package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.enumerated.RoleType;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.SQLException;
import java.util.Collection;

public interface ISessionRepository {

    void persist(@NotNull final Session session) throws Exception;

    void merge(@NotNull final Session session) throws Exception;

    void update(String sessionId, RoleType roleType, String userId, String signature, Long timestamp) throws SQLException;

    Session findSessionById(String sessionId) throws SQLException, Exception;

    Session findSessionByUserId(String userId) throws SQLException, Exception;

    void remove(@NotNull String session) throws Exception;

}
