package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.entity.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;
import java.util.List;

@WebService
public interface ITaskEndpoint {
    @WebMethod
    void createTask(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "task") @NotNull Task task,
                    @WebParam(name = "dateStart") XMLGregorianCalendar dateStart, @WebParam(name = "dateEnd") XMLGregorianCalendar dateEnd,
                    @WebParam(name = "dateSystem") @NotNull XMLGregorianCalendar dateSystem) throws java.lang.Exception;

//    @WebMethod
//    Collection<Task> getCollection(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    void loadListTask(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "list") @NotNull List<Task> list) throws java.lang.Exception;
    @WebMethod
    void updateTaskName(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "taskId") @NotNull String taskId, @WebParam(name = "taskName") @NotNull String taskName) throws java.lang.Exception;

    @WebMethod
    void deleteTasksByIdProject(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "projectId") @NotNull String projectId) throws java.lang.Exception;

    @WebMethod
    Collection<Task> findAllTasksByUserId(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    Collection<Task> findAllTasksByProjectId(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "projectId") @NotNull String projectId) throws java.lang.Exception;

    @WebMethod
    void deleteTask(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "taskId") @NotNull String taskId) throws  java.lang.Exception;

    @WebMethod
    void deleteAllTaskByUserId(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    void deleteAllTaskByProjectId(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "projectId") @NotNull String projectId) throws java.lang.Exception;

    @WebMethod
    Task findTaskByName(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "taskName") @NotNull String taskName) throws java.lang.Exception;

    @WebMethod
    List<Task> sortTasksByEndDate(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    List<Task> sortTasksByStartDate(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    List<Task> sortTasksBySystemDate(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    List<Task> sortTasksByStatus(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

//    @WebMethod
//    void addAllTasks(@WebParam(name = "tasks") @NotNull List<Task> tasks) throws java.lang.Exception;
}
