package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Task;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyCollectionException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface ITaskService {

    void create(@NotNull  String userId, @NotNull final Task task) throws Exception;

    void load(@NotNull List<Task> list) throws Exception;

    void updateTaskName(@NotNull String userId, @NotNull String taskId, @NotNull  String taskName) throws Exception;

    void deleteTaskByIdProject(@NotNull String userId, @NotNull String projectId) throws Exception;

    Collection<Task> findAllByUserId(@NotNull String userId) throws Exception;

    Collection<Task> findAllByProjectId(@NotNull String projectId) throws Exception;

    void deleteTaskByIdUser(@NotNull String userId, @NotNull String taskId) throws Exception;

    void deleteAllTaskByUserId(@NotNull String userId) throws Exception;

    void deleteAllTaskByProjectId(@NotNull String userId, @NotNull String projectId) throws Exception;

    Task findTaskByName(@NotNull String userId,@NotNull String taskName) throws Exception;

    List<Task> sortTasksByEndDate(@NotNull String userId) throws Exception;

    List<Task> sortTasksByStartDate(@NotNull String userId) throws Exception;

    List<Task> sortTasksBySystemDate(@NotNull String userId) throws Exception;

    List<Task> sortTasksByStatus(@NotNull String userId) throws Exception;

}
