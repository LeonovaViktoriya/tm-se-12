package ru.leonova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.User;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    void persist(@NotNull User user) throws SQLException;

    void updateLogin(@NotNull User user, @NotNull String login) throws SQLException;

    User findOne(@NotNull String userId) throws Exception;

    User findByLoginAndPassword(@NotNull String login, @NotNull String password) throws Exception;

    Collection<User> findAll() throws SQLException;

    void remove(@NotNull User user) throws Exception;

    void removeAll() throws SQLException;

    void load(@NotNull List<User> list) throws Exception;

    void updatePassword(@NotNull User user, @NotNull String password) throws Exception;
}
