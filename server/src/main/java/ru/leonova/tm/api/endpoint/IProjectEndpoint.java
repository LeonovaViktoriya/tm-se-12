package ru.leonova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Project;
import ru.leonova.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Collection;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    void createProject(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "project") @NotNull Project project,
                       @WebParam(name = "dateStart") XMLGregorianCalendar dateStart, @WebParam(name = "dateEnd") XMLGregorianCalendar dateEnd,
                       @WebParam(name = "dateSystem") @NotNull XMLGregorianCalendar dateSystem) throws java.lang.Exception;

    @WebMethod
    void updateNameProject(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "projectId") @NotNull String projectId, @WebParam(name = "name") @NotNull String name) throws java.lang.Exception;

    @WebMethod
    Collection<Project> findAllProjectsByUserId(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    void updateStatusProject(Session session, String projectId, String name) throws java.lang.Exception;

    @WebMethod
    void deleteProject(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "projectId") @NotNull String projectId) throws java.lang.Exception;

    @WebMethod
    void deleteAllProject(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    List<Project> sortProjectsBySystemDate(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    List<Project> sortProjectsByStartDate(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    List<Project> sortProjectsByEndDate(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    List<Project> sortProjectsByStatus(@WebParam(name = "session") @NotNull Session session) throws java.lang.Exception;

    @WebMethod
    Project searchProjectByName(@WebParam(name = "session") @NotNull Session session, @WebParam(name = "projectName") @NotNull String projectName) throws java.lang.Exception;
}
