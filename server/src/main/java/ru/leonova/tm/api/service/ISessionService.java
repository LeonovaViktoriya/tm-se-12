package ru.leonova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.leonova.tm.entity.Session;
import ru.leonova.tm.exeption.AccessException;
import ru.leonova.tm.exeption.EmptyArgumentException;

import java.sql.SQLException;

public interface ISessionService {
    boolean valid(@NotNull Session session) throws Exception;

    void closeSession(@NotNull Session session) throws Exception;

    Session openSession(@NotNull String login, @NotNull String password) throws Exception;

    Session getSessionByUserId(String userId) throws SQLException, Exception;
}
